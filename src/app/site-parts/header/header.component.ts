import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'work-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  name = '';
  nameSubscription!: Subscription;
  img = '/assets/bg.png';

  constructor(
    private authService: AuthService,
    public translate: TranslateService
  ) {
    translate.addLangs(['bg', 'en']);
    translate.setDefaultLang('en');

    const browserLang = translate.getBrowserLang();
    translate.use('en');
  }

  ngOnInit(): void {
    this.nameSubscription = this.authService.userName.subscribe((name) => {
      this.name = name;
    });
  }

  ngOnDestroy(): void {
    if (this.nameSubscription) {
      this.nameSubscription.unsubscribe();
    }
  }

  // logout
  logOut() {
    this.name = '';
    this.authService.logOut();
  }

  onLangChange() {
    const lang = this.translate.currentLang;
    if (lang === 'en') {
      this.translate.use('bg');
      this.img = '/assets/eng.png';
    } else if (lang === 'bg') {
      this.translate.use('en');
      this.img = '/assets/bg.png';
    }
  }
}
