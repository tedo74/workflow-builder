import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (this.authService.isTokenValid()) {
      let authToken = 'Bearer ';
      const token = this.authService.getToken();
      const cloned = req.clone({
        headers: req.headers
          .set('Content-Type', 'application/json')
          .set('Authorization', authToken + token),
      });
      return next.handle(cloned);
    } else {
      const cloned = req.clone({
        headers: req.headers.set('Content-Type', 'application/json'),
      });
      return next.handle(cloned);
    }
  }
}
