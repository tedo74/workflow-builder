export interface IUserModel {
  id: number;
  created?: Date;
  lastUpdated?: Date;
  userName: string;
  password: string;
  email: string;
}
