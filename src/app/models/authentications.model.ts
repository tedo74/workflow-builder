export interface IAuthentications {
  [key: string]: any;
  id: number;
  created: Date;
  lastUpdated: Date;
  userid: number;
  name: string;
  description: string;
  serviceName: string;
  data: {
    token: string;
    username: string;
  };
}
