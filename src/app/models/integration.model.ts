export interface IntegrationModel {
  id: number;
  created: Date;
  lastUpdated: Date;
  path: string;
  name: string;
  displayName?: string;
  description?: string;
}
