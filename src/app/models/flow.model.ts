export interface IFlowModel {
  id: number;
  path: string;
  name: string;
  process: {
    variables: IFlowVariables[];
  };
  meta: {
    type: string;
    info: string;
  };
}

export interface IFlowVariables {
  name: string;
  required: boolean;
  isInput: boolean;
  isOutput: boolean;
  schema: { type: string };
  meta?: {
    authType?: string;
    displayName?: string;
    description?: string;
  };
}
