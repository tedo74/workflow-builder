export interface IAuthschemas {
  serviceName: string;
  title: string;
  schema: {
    type: string;
    title?: string;
    properties: { [key: string]: IAuthSchemaProperty };
    required: string[];
  };
}

export interface IAuthSchemaProperty {
  type: string;
  title: string;
  enum?: string[];
  default?: string;
  description?: string;
  format?: string;
}
