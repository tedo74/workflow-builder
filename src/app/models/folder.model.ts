import { IntegrationModel } from './integration.model';

export interface IFolder {
  pathName: string;
  children: IFolder[];
  service?: IntegrationModel;
}
