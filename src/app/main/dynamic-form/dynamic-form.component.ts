import {
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { IFlowModel } from 'src/app/models/flow.model';
import { FlowsService } from '../flows.service';

@Component({
  selector: 'work-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss'],
})
export class DynamicFormComponent implements OnInit, OnDestroy {
  flowSubscription!: Subscription;
  flow!: IFlowModel;
  myForm: FormGroup = this.fb.group({});
  constructor(
    private flowService: FlowsService,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.flowSubscription = this.flowService.flowData
      .pipe(take(1))
      .subscribe((flow) => {
        this.flow = flow;
        this.createForm(flow);
        this.cdr.detectChanges();
      });
  }

  ngOnDestroy(): void {
    if (this.flowSubscription) {
      this.flowSubscription.unsubscribe();
    }
  }

  createForm(flow: IFlowModel) {
    let variables = flow.process.variables;
    for (let i = 0; i < variables.length; i++) {
      if (variables[i].isInput) {
        this.myForm.addControl(variables[i].name, this.fb.control('', []));
      }
    }
  }

  submitForm(myForm: FormGroup) {
    console.log(myForm.value);
  }
}
