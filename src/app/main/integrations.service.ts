import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IntegrationModel } from '../models/integration.model';

@Injectable({
  providedIn: 'root',
})
export class IntegrationsService {
  constructor(private httpClient: HttpClient) {}

  getIntegrations(): Observable<IntegrationModel[]> {
    return this.httpClient.get<IntegrationModel[]>(
      `http://localhost:3000/integrations`
    );
  }
}
