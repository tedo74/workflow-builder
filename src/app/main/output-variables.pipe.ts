import { Pipe, PipeTransform } from '@angular/core';
import { IFlowVariables } from '../models/flow.model';

@Pipe({
  name: 'outputVariables',
})
export class OutputVariablesPipe implements PipeTransform {
  transform(value: IFlowVariables[], ...args: unknown[]): IFlowVariables[] {
    return value.filter((flow) => flow.isOutput);
  }
}
