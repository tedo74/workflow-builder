import { Component, Input, OnInit } from '@angular/core';
import { IFolder } from 'src/app/models/folder.model';
import { IntegrationModel } from 'src/app/models/integration.model';

@Component({
  selector: 'work-folder',
  templateUrl: './folder.component.html',
  styleUrls: ['./folder.component.scss'],
})
export class FolderComponent implements OnInit {
  @Input() folderName!: string;
  @Input() children!: IFolder[];
  @Input() service!: IntegrationModel;
  showChildren = false;
  showChildrenOfChildren = false;
  childServices: IFolder[] = [];
  constructor() {}

  ngOnInit(): void {}

  toggleChildren() {
    this.showChildren = !this.showChildren;
  }
  toggleChildrenOfChildren() {
    this.showChildrenOfChildren = !this.showChildrenOfChildren;
  }
}
