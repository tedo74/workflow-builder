import {
  CdkDragDrop,
  copyArrayItem,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { IntegrationModel } from 'src/app/models/integration.model';
import { AuthsDialogWrappComponent } from 'src/app/serv-authentication/auths-dialog-wrapp/auths-dialog-wrapp.component';
import { FlowsService } from '../flows.service';

@Component({
  selector: 'work-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
  droppedList: IntegrationModel[] = [];
  errorMessage = '';

  constructor(private flowService: FlowsService, private dialog: MatDialog) {}

  ngOnInit(): void {}

  serviceDropped(event: CdkDragDrop<IntegrationModel[]>) {
    if (event.previousContainer !== event.container) {
      copyArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      let service = event.container.data[event.currentIndex];
      this.unique();

      if (service?.id) {
        this.flowService.getFlow(service.id).subscribe({
          next: (flow) => {
            this.flowService.flowData.next(flow);
          },
          error: (err) => {
            this.droppedList = this.droppedList.filter(
              (item) => item !== service
            );
            this.errorMessage = err.statusText;
            setTimeout(() => {
              this.errorMessage = '';
            }, 2000);
          },
        });
      }
    } else {
      moveItemInArray(
        this.droppedList,
        event.previousIndex,
        event.currentIndex
      );
    }
  }

  deleteService(item: IntegrationModel) {
    this.droppedList = this.droppedList.filter(
      (droped) => droped.id !== item.id
    );
  }

  unique() {
    this.droppedList = [...new Set(this.droppedList)];
  }

  showAuths() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    const dialogRef = this.dialog.open(AuthsDialogWrappComponent, dialogConfig);
  }
}
