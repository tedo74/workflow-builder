import { Component, Input, OnInit } from '@angular/core';
import { IntegrationModel } from 'src/app/models/integration.model';

@Component({
  selector: 'work-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.scss'],
})
export class ServiceComponent implements OnInit {
  @Input() serviceName!: string;
  @Input() service!: IntegrationModel;
  constructor() {}

  ngOnInit(): void {}
}
