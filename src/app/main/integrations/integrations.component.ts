import { Component, OnInit } from '@angular/core';
import { IFolder } from 'src/app/models/folder.model';
import { IntegrationModel } from 'src/app/models/integration.model';
import { IntegrationsService } from '../integrations.service';

@Component({
  selector: 'work-integrations',
  templateUrl: './integrations.component.html',
  styleUrls: ['./integrations.component.scss'],
})
export class IntegrationsComponent implements OnInit {
  tree: IFolder[] = [];
  errorMessage = '';
  constructor(private integrationsService: IntegrationsService) {}

  ngOnInit(): void {
    this.integrationsService.getIntegrations().subscribe({
      next: (integrations) => {
        this.tree = this.displayFolders(integrations);
      },
      error: (err) => {
        this.errorMessage = 'Create folders error';
        console.log(err);
      },
    });
  }

  pathToArray(path: string) {
    const paths: string[] = path.split('/').slice(1);
    return paths;
  }

  displayFolders(integrations: IntegrationModel[]) {
    const tree: IFolder[] = [];

    for (let i = 0; i < integrations.length; i++) {
      const path = this.pathToArray(integrations[i].path);
      let currentLevel: IFolder[] = tree;

      // loop single integration - path parts
      for (let j = 0; j < path.length; j++) {
        const part = path[j];

        // last element is service
        if (j === path.length - 1) {
          let name = path[path.length - 1];
          const displayName = integrations[i].displayName;

          if (displayName) {
            name = displayName;
          }

          const newService = {
            service: integrations[i],
            pathName: name,
            children: [],
          };

          currentLevel.push(newService);
          break;
        }

        let existingPath = this.findWhere(currentLevel, 'pathName', part);

        // if path exists move to next level
        if (existingPath) {
          currentLevel = existingPath.children;
        } else {
          // new part with children array
          const newFolder: IFolder = {
            pathName: part,
            children: [],
          };

          currentLevel.push(newFolder);
          // move down - next level
          currentLevel = newFolder.children;
        }
      }
    }
    return tree;
  }

  findWhere(array: IFolder[], pathName: string, value: string) {
    let counter = 0; // counter

    while (counter < array.length && array[counter].pathName !== value) {
      counter++;
    } // find the index where the id is the as the aValue

    if (counter < array.length) {
      // found and return place.
      return array[counter];
    } else {
      return false;
    }
  }
}
