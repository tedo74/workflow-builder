import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'work-dynamic-base',
  templateUrl: './dynamic-base.component.html',
  styleUrls: ['./dynamic-base.component.scss'],
})
export class DynamicBaseComponent {
  @Input() schema!: {
    type: string;
    enum?: string[];
  };
  @Input() placeholder!: string;
  @Input() controlName!: string;
  @Input() formGr!: FormGroup;
  @Input() hint!: string | undefined;
  @Input() authType!: string | undefined;
  @Input() required!: boolean;
  constructor() {}
}
