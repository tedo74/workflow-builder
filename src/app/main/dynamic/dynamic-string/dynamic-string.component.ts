import { Component, Input, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { DynamicBaseComponent } from '../dynamic-base/dynamic-base.component';

@Component({
  selector: 'work-dynamic-string',
  templateUrl: './dynamic-string.component.html',
  styleUrls: ['./dynamic-string.component.scss'],
})
export class DynamicStringComponent
  extends DynamicBaseComponent
  implements OnInit
{
  constructor(private fb: FormBuilder) {
    super();
  }
  myForm!: FormGroup;
  showHint = false;

  ngOnInit(): void {
    this.formGr.addControl(this.controlName, this.fb.control('', []));
    if (this.required) {
      this.formGr.get(this.controlName)?.setValidators([Validators.required]);
    }
  }

  trimHint(hint: string): string {
    if (hint.length > 30) {
      return hint.substring(0, 30) + '...';
    }
    return hint;
  }

  overHint() {
    this.showHint = true;
  }
  exitHint() {
    this.showHint = false;
  }
}
