import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DynamicBaseComponent } from '../dynamic-base/dynamic-base.component';

@Component({
  selector: 'work-dynamic-boolean',
  templateUrl: './dynamic-boolean.component.html',
  styleUrls: ['./dynamic-boolean.component.scss'],
})
export class DynamicBooleanComponent
  extends DynamicBaseComponent
  implements OnInit
{
  constructor(private fb: FormBuilder) {
    super();
  }

  ngOnInit(): void {
    this.formGr.addControl(this.controlName, this.fb.control('', []));
    if (this.required) {
      this.formGr
        .get(this.controlName)
        ?.setValidators([Validators.requiredTrue]);
    }
  }
}
