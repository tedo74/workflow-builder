import { Component, Type, OnInit } from '@angular/core';
import { DynamicBaseComponent } from '../dynamic-base/dynamic-base.component';
import { DynamicBooleanComponent } from '../dynamic-boolean/dynamic-boolean.component';
import { DynamicIntegerComponent } from '../dynamic-integer/dynamic-integer.component';
import { DynamicSelectComponent } from '../dynamic-select/dynamic-select.component';
import { DynamicStringComponent } from '../dynamic-string/dynamic-string.component';

@Component({
  selector: 'work-dynamic-wrapper',
  templateUrl: './dynamic-wrapper.component.html',
  styleUrls: ['./dynamic-wrapper.component.scss'],
})
export class DynamicWrapperComponent
  extends DynamicBaseComponent
  implements OnInit
{
  // constructor() {
  //   super();
  // }
  get component(): Type<DynamicBaseComponent> {
    if (this.authType || this.schema.enum) {
      return DynamicSelectComponent;
    } else {
      switch (this.schema.type) {
        case 'integer':
        case 'number':
          return DynamicIntegerComponent;
        case 'boolean':
          return DynamicBooleanComponent;
        default:
          return DynamicStringComponent;
      }
    }
  }

  ngOnInit(): void {}
}
