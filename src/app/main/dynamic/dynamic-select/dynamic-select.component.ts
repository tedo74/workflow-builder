import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { IAuthentications } from 'src/app/models/authentications.model';
import { AuthenticationsService } from 'src/app/serv-authentication/authentications.service';
import { DynamicBaseComponent } from '../dynamic-base/dynamic-base.component';

@Component({
  selector: 'work-dynamic-select',
  templateUrl: './dynamic-select.component.html',
  styleUrls: ['./dynamic-select.component.scss'],
})
export class DynamicSelectComponent
  extends DynamicBaseComponent
  implements OnInit
{
  authsSubscription!: Subscription;
  allCompatibleAuths: IAuthentications[] = [];

  constructor(
    private fb: FormBuilder,
    private authsService: AuthenticationsService
  ) {
    super();
  }

  ngOnInit(): void {
    this.formGr.addControl(this.controlName, this.fb.control('', []));
    if (this.required) {
      this.formGr.get(this.controlName)?.setValidators([Validators.required]);
    }
    if (this.authType) {
      if (this.authType !== 'Common/BasicAuth') {
        this.filterAuths();
      } else {
        this.authsService.getAuthentications().subscribe({
          next: (auths) => {
            this.allCompatibleAuths = auths;
          },
          error: (err) => {
            console.log(err);
          },
        });
      }
    }

    this.authsSubscription = this.authsService.AuthsChanged.subscribe(
      (auths) => {
        if (this.authType === 'Common/BasicAuth') {
          this.allCompatibleAuths = auths;
        } else {
          this.allCompatibleAuths = auths.filter(
            (item) => item.serviceName === this.authType
          );
        }
      }
    );
  }

  filterAuths() {
    this.authsService
      .getAuthentications()
      .pipe(
        map((items) => {
          return items.filter((item) => item.serviceName === this.authType);
        })
      )
      .subscribe({
        next: (auths) => {
          this.allCompatibleAuths = auths;
        },
        error: (err) => {
          console.log(err);
        },
      });
  }
}
