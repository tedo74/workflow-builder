import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DynamicBaseComponent } from '../dynamic-base/dynamic-base.component';

@Component({
  selector: 'work-dynamic-integer',
  templateUrl: './dynamic-integer.component.html',
  styleUrls: ['./dynamic-integer.component.scss'],
})
export class DynamicIntegerComponent
  extends DynamicBaseComponent
  implements OnInit
{
  showHint = false;
  @Input() placeholder!: string;

  constructor(private fb: FormBuilder) {
    super();
  }

  ngOnInit(): void {
    this.formGr.addControl(this.controlName, this.fb.control('', []));
    if (this.required) {
      this.formGr.get(this.controlName)?.setValidators([Validators.required]);
    }
  }

  trimHint(hint: string): string {
    if (hint.length > 30) {
      return hint.substring(0, 30) + '...';
    }
    return hint;
  }

  overHint() {
    this.showHint = true;
  }
  exitHint() {
    this.showHint = false;
  }
}
