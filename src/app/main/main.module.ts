import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { MainRoutingModule } from './main.routing.module';
import { IntegrationsComponent } from './integrations/integrations.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FolderComponent } from './folder/folder.component';
import { ServiceComponent } from './service/service.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InputVariablesPipe } from './input-variables.pipe';
import { OutputVariablesPipe } from './output-variables.pipe';
import { DynamicWrapperComponent } from './dynamic/dynamic-wrapper/dynamic-wrapper.component';
import { DynamicIntegerComponent } from './dynamic/dynamic-integer/dynamic-integer.component';
import { DynamicStringComponent } from './dynamic/dynamic-string/dynamic-string.component';
import { NgxdModule } from '@ngxd/core';
import { DynamicBaseComponent } from './dynamic/dynamic-base/dynamic-base.component';
import { DynamicSelectComponent } from './dynamic/dynamic-select/dynamic-select.component';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { DynamicBooleanComponent } from './dynamic/dynamic-boolean/dynamic-boolean.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import {
  MissingTranslationHandler,
  TranslateCompiler,
  TranslateLoader,
  TranslateModule,
  TranslateParser,
} from '@ngx-translate/core';

@NgModule({
  declarations: [
    MainComponent,
    IntegrationsComponent,
    FolderComponent,
    ServiceComponent,
    DynamicFormComponent,
    InputVariablesPipe,
    OutputVariablesPipe,
    DynamicWrapperComponent,
    DynamicIntegerComponent,
    DynamicStringComponent,
    DynamicBaseComponent,
    DynamicSelectComponent,
    DynamicBooleanComponent,
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    DragDropModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    NgxdModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
    MatCheckboxModule,
    MatDialogModule,

    TranslateModule.forChild({
      extend: true,
    }),
  ],
})
export class MainModule {}
