import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { take } from 'rxjs/operators';
import { IFlowModel } from '../models/flow.model';

@Injectable({
  providedIn: 'root',
})
export class FlowsService {
  flowData = new Subject<IFlowModel>();

  constructor(private httpClient: HttpClient) {}

  getFlow(id: number): Observable<IFlowModel> {
    return this.httpClient
      .get<IFlowModel>(`http://localhost:3000/flows/${id}`)
      .pipe(take(1));
  }
}
