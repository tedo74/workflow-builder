import { Pipe, PipeTransform } from '@angular/core';
import { IFlowVariables } from '../models/flow.model';

@Pipe({
  name: 'inputVariables',
})
export class InputVariablesPipe implements PipeTransform {
  transform(value: IFlowVariables[], ...args: unknown[]): IFlowVariables[] {
    return value.filter((flow) => flow.isInput);
  }
}
