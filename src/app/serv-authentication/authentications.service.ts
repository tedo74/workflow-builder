import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { take } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';
import { IAuthentications } from '../models/authentications.model';
import { IAuthschemas } from '../models/authschemas.model';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationsService {
  constructor(private httpClient: HttpClient, private authServ: AuthService) {}
  AuthsChanged = new Subject<IAuthentications[]>();

  getAuthentications(): Observable<IAuthentications[]> {
    const id = this.authServ.getUserId();
    return this.httpClient
      .get<IAuthentications[]>(
        `http://localhost:3000/authentications?userid=${id}`
      )
      .pipe(take(1));
  }
  getAuthItem(authId: number): Observable<IAuthentications> {
    const id = this.authServ.getUserId();
    return this.httpClient
      .get<IAuthentications>(`http://localhost:3000/auths/authId`)
      .pipe(take(1));
  }

  getAuthschemas(): Observable<IAuthschemas[]> {
    return this.httpClient
      .get<IAuthschemas[]>(`http://localhost:3000/schemas`)
      .pipe(take(1));
  }

  deleteAuth$(id: number): Observable<IAuthentications> {
    return this.httpClient
      .delete<IAuthentications>(`http://localhost:3000/auths/${id}`)
      .pipe(take(1));
  }

  postAuth$(auth: {
    name: string;
    description: string;
    serviceName: string;
    data: Object;
    created: number;
    lastUpdated: number;
  }): Observable<IAuthentications> {
    const id = this.authServ.getUserId();
    const body = { ...auth, userid: +id };
    return this.httpClient
      .post<IAuthentications>(`http://localhost:3000/auths`, body)
      .pipe(take(1));
  }

  editAuth$(auth: {
    id: number;
    name: string;
    description: string;
    serviceName: string;
    data: Object;
    created: number;
    lastUpdated: number;
  }): Observable<IAuthentications> {
    const id = this.authServ.getUserId();
    const body = { ...auth, userid: +id };
    return this.httpClient
      .put<IAuthentications>(`http://localhost:3000/auths/${+auth.id}`, body)
      .pipe(take(1));
  }
}
