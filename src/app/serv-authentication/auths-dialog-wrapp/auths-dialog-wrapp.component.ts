import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'work-auths-dialog-wrapp',
  templateUrl: './auths-dialog-wrapp.component.html',
  styleUrls: ['./auths-dialog-wrapp.component.scss'],
})
export class AuthsDialogWrappComponent implements OnInit {
  constructor(private dialogRef: MatDialogRef<AuthsDialogWrappComponent>) {}

  ngOnInit(): void {}

  close() {
    this.dialogRef.close(false);
  }
}
