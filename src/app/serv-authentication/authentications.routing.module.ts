import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllAuthenticationsComponent } from './all-authentications/all-authentications.component';

const routes: Routes = [{ path: '', component: AllAuthenticationsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthenticationsRoutingModule {}
