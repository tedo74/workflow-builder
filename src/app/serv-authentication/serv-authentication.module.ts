import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllAuthenticationsComponent } from './all-authentications/all-authentications.component';
import { AuthenticationsRoutingModule } from './authentications.routing.module';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { ConfirmDeleteComponent } from './confirm-delete/confirm-delete.component';
import { CreateAuthComponent } from './create-auth/create-auth.component';
import { PropertyBaseComponent } from './dynamic/property-base/property-base.component';
import { PropertyWrapperComponent } from './dynamic/property-wrapper/property-wrapper.component';
import { PropertySelectComponent } from './dynamic/property-select/property-select.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxdModule } from '@ngxd/core';
import { DynamicInputComponent } from './dynamic/dynamic-input/dynamic-input.component';
import { AuthsDialogWrappComponent } from './auths-dialog-wrapp/auths-dialog-wrapp.component';
import { TranslateModule } from '@ngx-translate/core';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [
    AllAuthenticationsComponent,
    ConfirmDeleteComponent,
    CreateAuthComponent,
    PropertyBaseComponent,
    PropertyWrapperComponent,
    PropertySelectComponent,
    DynamicInputComponent,
    AuthsDialogWrappComponent,
  ],
  imports: [
    CommonModule,
    AuthenticationsRoutingModule,
    NgxdModule,
    MatFormFieldModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSortModule,
    MatTableModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    ReactiveFormsModule,

    TranslateModule.forChild({
      extend: true,
    }),
  ],
})
export class ServAuthenticationModule {}
