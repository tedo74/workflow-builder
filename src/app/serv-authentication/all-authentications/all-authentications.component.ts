import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { take } from 'rxjs/operators';
import { IAuthentications } from 'src/app/models/authentications.model';
import { AuthenticationsService } from '../authentications.service';
import { ConfirmDeleteComponent } from '../confirm-delete/confirm-delete.component';
import { CreateAuthComponent } from '../create-auth/create-auth.component';

@Component({
  selector: 'work-all-authentications',
  templateUrl: './all-authentications.component.html',
  styleUrls: ['./all-authentications.component.scss'],
})
export class AllAuthenticationsComponent implements OnInit, AfterViewInit {
  dataSource = new MatTableDataSource<IAuthentications>([]);
  displayedColumns: string[] = [
    'name',
    'serviceName',
    'data.username',
    'description',
    'edit',
  ];
  @ViewChild(MatSort) sort!: MatSort;
  filterForm!: FormGroup;

  constructor(
    private authenticationsServ: AuthenticationsService,
    private dialog: MatDialog,
    fb: FormBuilder
  ) {
    this.filterForm = fb.group({
      search: '',
      byName: false,
      byDescription: false,
      byUserName: false,
      byService: false,
    });
  }

  ngOnInit(): void {
    this.getAuths();
    this.defaultFilter();
    this.filterForm.valueChanges.subscribe((checked) => {
      if (
        checked.byName === false &&
        checked.byDescription === false &&
        checked.byUserName === false &&
        checked.byService === false
      ) {
        this.defaultFilter();
        this.search(checked.search);
      } else {
        this.dataSource.filterPredicate = (item, filter) => {
          if (
            (checked.byUserName &&
              item.data.username.toLowerCase().indexOf(filter) !== -1) ||
            (checked.byName &&
              item.name.toLowerCase().indexOf(filter) !== -1) ||
            (checked.byDescription &&
              item.description.toLowerCase().indexOf(filter) !== -1) ||
            (checked.byService &&
              item.serviceName.toLowerCase().indexOf(filter) !== -1)
          ) {
            return true;
          }
          return false;
        };

        this.search(checked.search);
      }
    });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'data.username':
          return item.data.username.toLowerCase();
        default:
          return item[property].toLowerCase();
      }
    };
  }

  getAuths() {
    this.authenticationsServ.getAuthentications().subscribe({
      next: (auths) => {
        this.dataSource.data = auths;
        this.authenticationsServ.AuthsChanged.next(auths);
      },
      error: (err) => {
        console.log(err);
      },
    });
  }

  editAuth(row: IAuthentications) {
    this.createAuth(row);
  }

  deleteAuth(row: IAuthentications) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    const dialogRef = this.dialog.open(ConfirmDeleteComponent, dialogConfig);
    dialogRef
      .afterClosed()
      .pipe(take(1))
      .subscribe({
        next: (result: boolean) => {
          if (result) {
            this.authenticationsServ.deleteAuth$(row.id).subscribe(() => {
              this.dataSource.data = this.dataSource.data.filter(
                (item) => item.id !== row.id
              );
              this.authenticationsServ.AuthsChanged.next(this.dataSource.data);
            });
          }
          return;
        },
      });
  }

  createAuth(authentication?: IAuthentications) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    // on edit
    dialogConfig.data = authentication;
    const dialogRef = this.dialog.open(CreateAuthComponent, dialogConfig);
    dialogRef
      .afterClosed()
      .pipe(take(1))
      .subscribe({
        next: () => {
          this.getAuths();
        },
        error: (err) => {
          console.log(err);
        },
      });
  }

  search(searchValue = '') {
    const value = searchValue.toLocaleLowerCase().trim();
    this.dataSource.filter = value;
  }

  getServiceImgOrName(username: string): string {
    const tempUsername = username.toLowerCase().trim();
    if (tempUsername === 'slack') {
      return '../../../assets/slack.png';
    } else if (tempUsername === 'atlassian') {
      return '../../../assets/atlassian.png';
    }
    return username;
  }

  defaultFilter() {
    this.dataSource.filterPredicate = (item, filter) => {
      if (
        item.data.username.toLowerCase().indexOf(filter) !== -1 ||
        item.name.toLowerCase().indexOf(filter) !== -1 ||
        item.description.toLowerCase().indexOf(filter) !== -1 ||
        item.serviceName.toLowerCase().indexOf(filter) !== -1
      ) {
        return true;
      }
      return false;
    };
  }
}
