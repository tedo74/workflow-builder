import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from 'src/app/auth/auth.service';
import { IAuthentications } from 'src/app/models/authentications.model';
import { IAuthschemas } from 'src/app/models/authschemas.model';
import { AuthenticationsService } from '../authentications.service';

@Component({
  selector: 'work-create-auth',
  templateUrl: './create-auth.component.html',
  styleUrls: ['./create-auth.component.scss'],
})
export class CreateAuthComponent implements OnInit {
  authSchemas: IAuthschemas[] = [];
  selected!: IAuthschemas;
  authServForm!: FormGroup;
  authentication!: IAuthentications;
  buttonName = 'create';

  constructor(
    @Inject(MAT_DIALOG_DATA) data: IAuthentications,
    private authService: AuthenticationsService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<CreateAuthComponent>,
    private userService: AuthService,
    private cdr: ChangeDetectorRef
  ) {
    this.authentication = data;
  }

  ngOnInit(): void {
    this.authService.getAuthschemas().subscribe({
      next: (schemas) => {
        this.authSchemas = schemas;
        this.selected = this.authSchemas[0];
        this.createForm();

        // edit
        if (this.authentication) {
          this.selected =
            this.authSchemas.find(
              (element) =>
                element.serviceName === this.authentication.serviceName
            ) || this.selected;
          this.setValues();
          this.cdr.detectChanges();
        }
      },
      error: (err) => {
        console.log(err);
        this.userService.logOut();
      },
    });
  }

  onEdit(f: FormGroup) {
    const { description, service, ...data } = f.value;
    const name = this.authentication.name;

    const body = {
      id: this.authentication.id,
      name,
      description,
      serviceName: this.selected.serviceName,
      data,
      created: +this.authentication.created,
      lastUpdated: new Date().getTime(),
    };

    this.authService.editAuth$(body).subscribe();
  }

  onCreate(f: FormGroup) {
    const { name, description, service, ...data } = f.value;
    const body = {
      name,
      description,
      serviceName: service.serviceName,
      data,
      created: new Date().getTime(),
      lastUpdated: new Date().getTime(),
    };
    this.authService.postAuth$(body).subscribe();
  }

  setValues() {
    this.buttonName = 'edit';
    this.authServForm.setValue({
      name: this.authentication.name,
      description: this.authentication.description,
      service: this.selected || [],
    });
    this.authServForm.get('name')?.disable();
  }

  onChange(value: IAuthschemas) {
    const name = this.authServForm.controls['name'].value;
    const description = this.authServForm.controls['description'].value;
    this.createForm();
    this.authServForm.setValue({
      name,
      description,
      service: value,
    });
    this.selected = value;
    this.cdr.detectChanges();
  }

  createForm() {
    this.authServForm = this.fb.group({
      name: [
        { value: '', disabled: false },
        { validators: [Validators.required] },
      ],
      description: ['', { validators: [Validators.required] }],
      service: [this.selected, { validators: [Validators.required] }],
    });
  }

  close() {
    this.dialogRef.close(false);
  }

  onSubmit(f: FormGroup) {
    if (!this.authentication) {
      this.onCreate(f);
    } else {
      this.onEdit(f);
    }
    this.dialogRef.close(true);
  }
}
