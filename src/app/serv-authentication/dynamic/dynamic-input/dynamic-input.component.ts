import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { PropertyBaseComponent } from '../property-base/property-base.component';

@Component({
  selector: 'work-dynamic-input',
  templateUrl: './dynamic-input.component.html',
  styleUrls: ['./dynamic-input.component.scss'],
})
export class DynamicInputComponent
  extends PropertyBaseComponent
  implements OnInit
{
  type = 'text';
  constructor(private fb: FormBuilder) {
    super();
  }

  ngOnInit(): void {
    this.type = this.property.type;
    if (this.type === 'integer') {
      this.type = 'number';
    }
    if (this.property.format) {
      this.type = this.property.format;
    }
    this.formGrp.addControl(
      this.controlName,
      this.fb.control('', { validators: [] })
    );
    if (this.required.includes(this.controlName)) {
      this.formGrp.get(this.controlName)?.setValidators([Validators.required]);
    }
  }
}
