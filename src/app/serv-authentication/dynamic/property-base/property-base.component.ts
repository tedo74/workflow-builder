import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { IAuthSchemaProperty } from 'src/app/models/authschemas.model';

@Component({
  selector: 'work-property-base',
  templateUrl: './property-base.component.html',
  styleUrls: ['./property-base.component.scss'],
})
export class PropertyBaseComponent implements OnInit {
  @Input() property!: IAuthSchemaProperty;
  @Input() formGrp!: FormGroup;
  @Input() controlName!: string;
  @Input() required!: string[];
  constructor() {}

  ngOnInit(): void {}
}
