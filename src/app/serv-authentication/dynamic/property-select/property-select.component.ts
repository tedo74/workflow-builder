import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { PropertyBaseComponent } from '../property-base/property-base.component';

@Component({
  selector: 'work-property-select',
  templateUrl: './property-select.component.html',
  styleUrls: ['./property-select.component.scss'],
})
export class PropertySelectComponent
  extends PropertyBaseComponent
  implements OnInit
{
  constructor(private fb: FormBuilder) {
    super();
  }

  ngOnInit(): void {
    this.formGrp.addControl(
      this.controlName,
      this.fb.control('', { validators: [] })
    );
    if (this.required.includes(this.controlName)) {
      this.formGrp.get(this.controlName)?.setValidators([Validators.required]);
    }
  }
}
