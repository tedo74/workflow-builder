import { Component, OnInit, Type } from '@angular/core';
import { DynamicInputComponent } from '../dynamic-input/dynamic-input.component';
import { PropertyBaseComponent } from '../property-base/property-base.component';
import { PropertySelectComponent } from '../property-select/property-select.component';

@Component({
  selector: 'work-property-wrapper',
  templateUrl: './property-wrapper.component.html',
  styleUrls: ['./property-wrapper.component.scss'],
})
export class PropertyWrapperComponent
  extends PropertyBaseComponent
  implements OnInit
{
  // constructor() { }

  ngOnInit(): void {}
  get component(): Type<PropertyBaseComponent> {
    if (this.property.enum) {
      return PropertySelectComponent;
    } else {
      return DynamicInputComponent;

      // switch (this.property.type) {
      //   case 'integer':
      //     return DynamicInputComponent;
      //   case 'boolean':
      //     return DynamicInputComponent;
      //   default:
      //     return DynamicInputComponent;
      // }
    }
  }
}
