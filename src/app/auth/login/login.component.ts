import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormControlName,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'work-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  // show error message in template
  errorMessage = '';

  // form
  loginForm!: FormGroup;

  constructor(
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    public translate: TranslateService
  ) {}

  // used for validation in template.
  get email(): AbstractControl {
    return this.loginForm.controls['email'];
  }
  get password(): AbstractControl {
    return this.loginForm.controls['password'];
  }

  ngOnInit(): void {
    this.loginForm = this.formCreator();
  }

  // return form
  formCreator(): FormGroup {
    return this.fb.group({
      email: [
        '',
        {
          validators: [
            Validators.required,
            Validators.email,
            Validators.pattern('^[A-Za-z0-9._]+@[A-Za-z0-9._]+\\.[a-z]{2,3}$'),
          ],
          updateOn: 'blur',
        },
      ],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  // submit form
  onLoginHandler() {
    this.authService.login$(this.loginForm.value).subscribe({
      next: (data) => {
        this.router.navigate(['/main']);
        this.authService.userName.next(data.user.userName);
      },
      error: (err) => {
        if (
          err.error === 'Cannot find user' ||
          err.error === 'Incorrect password'
        ) {
          this.errorMessage = 'Check email or password';
        } else {
          this.errorMessage = 'Server error';
        }
      },
    });
  }
}
