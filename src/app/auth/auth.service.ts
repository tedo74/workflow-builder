import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, Subject } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { IUserModel } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  userName = new Subject<string>();
  private helper = new JwtHelperService();

  constructor(private httpClient: HttpClient, private router: Router) {}

  // token methods:
  isTokenValid(): boolean {
    const token = this.getToken();
    if (!token || this.helper.isTokenExpired(token)) {
      return false;
    } else {
      return true;
    }
  }

  getToken(): string {
    return localStorage.getItem('token') || '';
  }

  setToken(token: string): void {
    localStorage.setItem('token', token);
  }

  // get user id
  getUserId(): string {
    let id = '';
    if (this.isTokenValid()) {
      let token = this.getToken();
      try {
        id = this.helper.decodeToken(token).sub;
      } catch (error) {
        this.logOut();
      }
    } else {
      this.logOut();
    }
    return id;
  }

  // login
  login$(data: {
    email: string;
    password: string;
  }): Observable<{ user: IUserModel }> {
    const body = {
      email: data.email,
      password: data.password,
    };

    return this.httpClient
      .post<{ accessToken: string; user: IUserModel }>(
        `http://localhost:3000/login`,
        body
      )
      .pipe(
        take(1),
        tap((data) => this.setToken(data.accessToken))
      );
  }

  // logout
  logOut(): void {
    localStorage.removeItem('token');
    location.reload();
    this.router.navigate(['user', 'login']);
  }

  // patch user lastUpdated property - not used
  updateUser$(id: string) {
    let body = { lastUpdated: new Date() };
    return this.httpClient.patch(`http://localhost:3000/users/${id}`, body);
  }
}
